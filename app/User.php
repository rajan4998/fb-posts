<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Http\Models\SSOLogin;
use Hash;
use Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','gender','profile_image','access_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function check_user($sso_user, $provider)
    {
        //checking if user email id exists in our system
        $user = User::where('email', $sso_user['email'])->first();
        //if email returns a null object
        if ($user==null) {
            //create user against new available data
            $new_user = User::create([
                'name' => $sso_user['name'],
                'email' => $sso_user['email'],
                'gender' => $sso_user['gender'],
                'password' => Hash::make('Demo@123'),
                'profile_image' => $sso_user['profile_image'],
            ]);
            //creating sso data and inserting fb id
            SSOLogin::create([
                'user_id' => $new_user->id,
                'provider' => $provider,
                'provider_id' => $sso_user['id'],
                'provider_access_token' => $sso_user['access_token']
            ]);
            //getting user logged into system
            return User::custom_login($new_user);
        }
        //if user email already exists in the system
        if ($user!=null) {
            //getting user logged into system
            return User::custom_login($user);
        }
    }

    public static function custom_login($user)
    {
        //getting user logged in
        Auth::login($user);
        //checking if user logged in successfully
        if (Auth::check()) {
            //returning success status and message
            return response()->json(['status' => 1,'message' => 'login successfull']);
        } else {
            //returning login failed status and message
            return response()->json(['status' => 0,'message' => 'login failed']);
        }
    }
}
