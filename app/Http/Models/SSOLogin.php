<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class SSOLogin extends Model
{
    protected $table = "social_logins";
    protected $fillable = ['user_id','provider','provider_id','provider_access_token'];
}
