<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\Post;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $r)
    {
        $posts = Post::where('user_id', $r->user()->id)->get();
        dd($posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $post_image = null;
        $form_data = $r->all();

        if ($form_data['title']=="") {
            //redirecting user if there's no image
            session(['message' => 'Title is required.']);
        }
        //checking if request has a file
        if ($r->hasFile('post_image')) {
            //getting filename
            $file = $r->file('post_image');
            //getting file extensions
            $ext = $file->getClientOriginalExtension();
            //setting uploading path for the image
            $filepath = 'uploads/user_'.$r->user()->id.'/';
            //creating file name dynamically to avoid over write of images
            $post_image = $form_data['title'].'_'.time().'.'.$ext;
            //moving uploaded file to destined folder
            $file->move($filepath, $post_image);
            //setting path for db value
            $post_image = '/'.$filepath.''.$post_image;
        } else {
            //redirecting user if there's no image
            session(['message' => 'Please upload an image.']);

            return redirect()->back();
        }

        //validations left
        //creating db value
        Post::create([
            'user_id' => $r->user()->id,
            'title' => $form_data['title'] != '' ? $form_data['title'] : null,
            'post_time' => $form_data['date'] != null ? $form_data['date'] : \Carbon\Carbon::now(),
            'image' => $post_image != null ? $post_image : null
        ]);

        //return redirect
        session(['message' => 'Post created successfully.']);
        return redirect()->back();
        // return redirect('/posts');
    }
}
