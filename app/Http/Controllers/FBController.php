<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Socialite;
use App\User;

class FBController extends Controller
{
    /**
    * Redirect the user to the GitHub authentication page.
    *
    * @return Response
    */
    public function redirectToProvider()
    {
        //redirect to fb login page
        return Socialite::driver('facebook')->redirect();
    }

    /**
    * Obtain the user information from GitHub.
    *
    * @return Response
    */
    public function handleProviderCallback(Request $r)
    {
        //trying to get fb logged in user details
        //and throwing exception is login fails
        try {
            //getting user details
            $fb_user = Socialite::driver('facebook')->stateless()->user();
        } catch (\Exception $e) {
            //thrwoing 404 if there's an exception
            abort(404);
        }
        //checking if the returned user data is not null
        if ($fb_user!=null) {
            //getting nested user data from object
            $user_details = $fb_user->user;
            //getting user data from fb and creating user array
            $mapped_user = [
                'id' => $user_details['id'],
                'access_token' => $fb_user->token,
                'name' => (isset($user_details['name']) && $user_details['name']!=null) ? $user_details['name'] : null,
                'email' => (isset($user_details['email']) && $user_details['email']!=null) ? $user_details['email'] : null,
                'gender' => (isset($user_details['email']) && $user_details['email']!=null) ? $user_details['email'] : null,
                'profile_image' => ($fb_user->avatar_original!=null) ? $fb_user->avatar_original : null,
            ];
            //checking User status and existence in our system
            //redirecting back tp dashboard if
            $user_status = User::check_user($mapped_user, 'facebook');
            //checking returning status code
            if ($user_status->original['status']==1) {
                //return redirecting to home
                return redirect('/home');
            } else {
                //return redirecting to login if registeration failed
                return redirect('/login');
            }
        }
    }
}
