<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\Post;
use App\Http\Models\SSOLogin;

class DemoController extends Controller
{
    public function get_posts(Request $r)
    {
        $posts = Post::whereBetween('post_time', [\Carbon\Carbon::now(), \Carbon\Carbon::now()->addMinutes(5)])->get();
        dd($posts);
    }
    public function get_user_data()
    {
        $fb = new \Facebook\Facebook([
          'app_id' => '1719628874775460',
          'app_secret' => 'feba69c90448d3a27d78de0dade20737',
          'default_graph_version' => 'v2.10',
          'default_access_token' => 'EAAYbZCkw5ZB6QBAKYniUs24uG4uxJ7YTT1Xcw1rj4ZAXZBZBM77FvqnPXcpaa5ZBDypVygPZBAtxmEnZCNZBO05NnKZCzZCwMQXA5bZA6wUNdX6ZC7ygiyLvSU6syPnv5cx3H0DziWj2EQkwRjHlIZC2krm5JI3BCd6gqc3h7wfnuyFvoZB4wZDZD', // optional
        ]);

        // Use one of the helper classes to get a Facebook\Authentication\AccessToken entity.
        //   $helper = $fb->getRedirectLoginHelper();
        //   $helper = $fb->getJavaScriptHelper();
        //   $helper = $fb->getCanvasHelper();
        //   $helper = $fb->getPageTabHelper();

        try {
            // Get the \Facebook\GraphNodes\GraphUser object for the current user.
            // If you provided a 'default_access_token', the '{access-token}' is optional.
            $response = $fb->get('/me', 'EAAYbZCkw5ZB6QBAKYniUs24uG4uxJ7YTT1Xcw1rj4ZAXZBZBM77FvqnPXcpaa5ZBDypVygPZBAtxmEnZCNZBO05NnKZCzZCwMQXA5bZA6wUNdX6ZC7ygiyLvSU6syPnv5cx3H0DziWj2EQkwRjHlIZC2krm5JI3BCd6gqc3h7wfnuyFvoZB4wZDZD');
            dd($response);
        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        $me = $response->getGraphUser();
        dd($me);
        echo 'Logged in as ' . $me->getName();
    }

    public function index()
    {
        $posts = Post::whereBetween('post_time', [\Carbon\Carbon::now(), \Carbon\Carbon::now()->addMinutes(5)])->get();
        $users = SSOLogin::whereIn('user_id', $posts->pluck('user_id'))->whereNotNull('provider_id')->get()->keyBy('user_id');

        $posts->reject(function ($i) use ($users) {
            if (!file_exists(base_path().'/public'.''.$i->image) && !isset($users[$i->user_id])) {
                return $i;
            }
        })
        ->map(function ($i) use ($users) {
            $user = $users[$i->user_id];

            $fb = new \Facebook\Facebook([
              'app_id' => '1719628874775460',
              'app_secret' => 'feba69c90448d3a27d78de0dade20737',
              'default_graph_version' => 'v2.10'
            ]);

            $data = [
              'message' => $i->title,
              'source' => $fb->fileToUpload(base_path().'/public'.''.$i->image),
            ];

            try {
                // Returns a `Facebook\FacebookResponse` object
                $response = $fb->post('/me/photos', $data, $user->provider_access_token);
            } catch (Facebook\Exceptions\FacebookResponseException $e) {
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }

            if ($response!=null) {
                $graphNode = $response->getGraphNode();

                echo 'Photo ID: ' . $graphNode['id'];
            } else {
                return $response;
            }
        });
    }
}
