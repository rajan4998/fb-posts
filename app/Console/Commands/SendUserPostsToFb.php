<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Models\Post;
use App\Http\Models\SSOLogin;

class SendUserPostsToFb extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SendUserPostsToFb:sendposts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will post users posts created to fb at saved time.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $posts = Post::whereBetween('post_time', [\Carbon\Carbon::now(), \Carbon\Carbon::now()->addMinutes(5)])->get();
        $users = SSOLogin::whereIn('user_id', $posts->pluck('user_id'))->whereNotNull('provider_id')->get()->keyBy('user_id');

        $posts->reject(function ($i) use ($users) {
            if (!file_exists(base_path().'/public'.''.$i->image) && !isset($users[$i->user_id])) {
                return $i;
            }
        })
        ->map(function ($i) use ($users) {
            $user = $users[$i->user_id];

            $fb = new \Facebook\Facebook([
              'app_id' => '1719628874775460',
              'app_secret' => 'feba69c90448d3a27d78de0dade20737',
              'default_graph_version' => 'v2.10'
            ]);

            $data = [
              'message' => $i->title,
              'source' => $fb->fileToUpload(base_path().'/public'.''.$i->image),
            ];

            try {
                // Returns a `Facebook\FacebookResponse` object
                $response = $fb->post('/me/photos', $data, $user->provider_access_token);
            } catch (Facebook\Exceptions\FacebookResponseException $e) {
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }

            if ($response!=null) {
                $graphNode = $response->getGraphNode();

                echo 'Photo ID: ' . $graphNode['id'];
            } else {
                return $response;
            }
        });
    }
}
