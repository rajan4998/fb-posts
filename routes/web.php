<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//fb sso routes
Route::get('/', function () {
    return view('welcome');
});

Route::get('/login/fb', 'FBController@redirectToProvider');
Route::get('/fb/redirect', 'FBController@handleProviderCallback');

Route::get('/demo', ['uses' => 'DemoController@get_posts']);
Route::get('/get_posts', ['uses' => 'DemoController@index']);
Auth::routes();
//auth protected routes
Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('posts', 'PostsController', ['only' => ['index','create', 'store']]);
});
